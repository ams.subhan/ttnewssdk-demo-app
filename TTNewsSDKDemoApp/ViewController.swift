//
//  ViewController.swift
//  TTNewsSDKDemoApp
//
//  Created by MBP on 09/11/2020.
//

import UIKit
import TTNewsSDK

class ViewController: UIViewController {
    
    @IBOutlet weak var profileIdView: UITextField!

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        profileIdView.placeholder = "Add Profile ID"
        self.navigationController?.isNavigationBarHidden = true
        print(TTNews.sdkVersion)
        try! TTNews.setup(accessKey: "<ACCESS_KEY>", projectToken: "<PROJECT_TOKEN>")
    }

    @IBAction func gotoSDK(_ sender: Any) {
        if self.traitCollection.userInterfaceStyle == .dark {
            TTNews.appTheme = .dark
            
            TTNewsColors.navigationBarTint = "00CEBA"
            TTNewsColors.newsChannelBGColor = "#128390"
            TTNewsColors.newsCategoryBGColor = "#484848"
            TTNewsColors.channelSelectionColor = "212121"
            TTNewsColors.categorySelectionColor = "73FDFF"
            TTNewsColors.selectedTextColor = "FFFFFF"
            TTNewsColors.unSelectedTextColor = "e0e0e0"
            TTNewsColors.newsCardTextColor = "ffffff"
            TTNewsColors.newsViewBGColor = "000000"
            TTNewsColors.newsCardViewBGColor = "202020"
        } else {
            TTNews.appTheme = .light
            
            TTNewsColors.navigationBarTint = "00CEBA"
            TTNewsColors.newsChannelBGColor = "#128390"
            TTNewsColors.newsCategoryBGColor = "#D78390"
            TTNewsColors.channelSelectionColor = "212121"
            TTNewsColors.categorySelectionColor = "73FDFF"
            TTNewsColors.selectedTextColor = "000000"
            TTNewsColors.unSelectedTextColor = "212121"
            TTNewsColors.newsCardTextColor = "202020"
            TTNewsColors.newsViewBGColor = "e0e0e0"
            TTNewsColors.newsCardViewBGColor = "ffffff"
        }

        TTNews.setCurrentProfile(profileId: profileIdView.text ?? "12345")
        TTNews.presentNewsSDK(controller: self, delegate: self)
        
    }
    
    
    @IBAction func routeToCatAction(_ sender: Any) {
        try! TTNews.setup(accessKey: "<ACCESS_KEY>", projectToken: "<PROJECT_TOKEN>")

        TTNews.routeToCategory(controller: self, delegate: self, newsItem: NewsItemModel(profileId: "12345", categoryId: "dawn", subCategoryId: "Sport"))
    }
    
    @IBAction func viewSingleItemAction(_ sender: Any) {
//        TTNews.showNewsItem(controller: self, newsItem: NewsItemModel(profileId: "12345", categoryId: "dawn", subCategoryId: "Pakistan", newsId: "31092", newsTitle: "Headline text", newsURL: "https://www.dawn.com/news/1603929/broadsheet-is-fraudsheet-maryam-threatens-to-spill-the-beans-unless-azmat-saeed-recuses-himself-from-inquiry", channelName: "Dawn News", icon_url: ""))
        
    do {
        try TTNews.showNewsItem(controller: self, newsJSON: news_payload)
    }   catch {
            print(error)
        }
    }
    
}

extension ViewController: NewsVCDelegate{
    func dismissingView(isSessionExpired: Bool) {
        print("View about to be dismissed")
        print(isSessionExpired)
    }
    
    func viewDismissed(isSessionExpired: Bool) {
        print("View dismissed")
        print(isSessionExpired)
    }
}
