//
//  NewsJSON.swift
//  DemoApp
//
//  Created by Ali Mehdi on 04/03/2021.
//

import Foundation

var news_payload = """
{
  "data1": {
    "Body": {
      "newsFeedsRequest": {
        "ProfileId": "1090",
        "sdkNotification": {
          "News_id": "33281",
          "News_title": "PM approves Nullah Leh project with estimated cost of Rs75 billion",
          "News_description": "PM approves Nullah Leh project with estimated cost of Rs75 billion",
          "News_url":  "https://www.pakistantoday.com.pk/2021/02/12/pm-approves-nullah-leh-project-with-estimated-cost-of-rs75-billion/",
          "Category_id": "paktoday",
          "Sub_category_id": "National",
          "Channel_Name": "Pakistan Today",
          "icon_url":""
        }
      }
    },
    "header": {
      "ChannelIdentifier": "TILISM",
      "Token": "testing",
      "TransactionType": ""
    }
  },
  "VersionNo": "1",
  "ChannelID": "10"
}
"""
//"{\"data1\":{\"Body\":{\"newsFeedsRequest\":{\"ProfileId\":\"1090\",\"sdkNotification\":{\"News_id\":\"33281\",\"News_title\":\"PM approves Nullah Leh project with estimated cost of Rs75 billion\",\"News_description\":\"PM approves Nullah Leh project with estimated cost of Rs75 billion\n\",\"News_url\":\"\",\"Category_id\":\"paktoday\",\"Sub_category_id\":\"National\",\"Channel_Name\":\"Pakistan Today\",\"icon_url\":\"\"}}},\"header\":{\"ChannelIdentifier\":\"TILISM\",\"Token\":\"testing\",\"TransactionType\":\"\"}},\"VersionNo\":\"1\",\"ChannelID\":\"10\"}"
