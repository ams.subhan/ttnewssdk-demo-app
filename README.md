    # TelloTalk News SDK - iOS

TelloTalk News SDK is a solution from integrating news in your app.

## Pre Requisites
You must have **Access Key** & **Project Key** to use this SDK in your application.

## Installation
Add **TTNewsSDK.xcframework** file provided in framework directory into your application, then select your application target, in *General* tab, then add .framework file in *Embedded Binaries*

## Usage

### Initializaition
In order to begin you need to import SDK
```swift
import TTNewsSDK
```

### Setup SDK
First you need to setup sdk by providing access key and project token. You need to also call this method when you are directly viewing a news or news category.
```swift
TTNews.setup(accessKey: "<ACCESS_KEY>", projectToken: "<PROJECT_KEY>")
```

### Register User
When you're logging in for the application,  you need to call following method with a unique user profile ID.
```swift
TTNews.setCurrentProfile(profileId: "<PROFILE_ID>")
```

### View News Controller
To view news you must call following method passing presenting controller and NewsVCDelegate delegate class.

**Note:** You need to call TTNews.setup() method before viewing news view controller.

```swift
TTNews.presentNewsSDK(controller: UIViewController, delegate: NewsVCDelegate? = nil)
```

### Route to News Category
If you want to go to a particular news'  category, SDK needs to know user's profile id, category id and sub category id. You can provide these values in NewsItemModel class. By calling following method you can view news category. 

**Note:** You need to call TTNews.setup() method before viewing news category.

```swift
TTNews.routeToCategory(controller: UIViewController, delegate: NewsVCDelegate? = nil, newsItem: NewsItemModel)
```

### View News Item
If you want to directly view a particular news you must call below method by providing required parameters. This method requires a presenting view controller and NewsItemModel in which you must provide profile Id, category Id, sub category Id, news Id, news title and news URL.

**Note:** You need to call TTNews.setup() method before viewing news item.
```swift
TTNews.showNewsItem(controller: UIViewController?, newsItem: NewsItemModel)
```

### View News Item From JSON Payload
If you want to directly view a particular news you must call below method by providing required parameters. This method requires a presenting view controller and json payload.

**Note:** You need to call TTNews.setup() method before viewing news item.
```swift
TTNews.showNewsItem(controller: UIViewController?, newsJSON: String) throws
```

### Dismiss News View Delegates
If you want delegates for dismissing news controller you can confrim with NewsVCDelegate protocol and implement following two methods. In the methods you would receive either user session is expired or not.
**Note** Session time is 5min, after inactivity of 5 min, isSessionExpired value would be set true.
```swift
extension ViewController: NewsVCDelegate{
    func dismissingView(isSessionExpired: Bool) {
        print("View about to be dismissed")
    }
    
    func viewDismissed(isSessionExpired: Bool) {
        print("View dismissed")
    }
}
```

### Set App Theme
You can change app theme for dark and light modes by using following methods
```swift
TTNews.appTheme = .light
```

### Set Colors
You can set navigation bar tint color, background color of news channel background view, news channel and category selection color,  selected and unselected news channel and category text color, news text color, news card background color and news view background color. 
```swift

TTNewsColors.navigationBarTint = "<HEX_COLOR_CODE>"
TTNewsColors.newsChannelBGColor = "<HEX_COLOR_CODE>"
TTNewsColors.newsCategoryBGColor = "<HEX_COLOR_CODE>"
TTNewsColors.channelSelectionColor = "<HEX_COLOR_CODE>"
TTNewsColors.categorySelectionColor = "<HEX_COLOR_CODE>"
TTNewsColors.selectedTextColor = "<HEX_COLOR_CODE>"
TTNewsColors.unSelectedTextColor = "<HEX_COLOR_CODE>"
TTNewsColors.newsCardTextColor = "<HEX_COLOR_CODE>"
TTNewsColors.newsViewBGColor = "<HEX_COLOR_CODE>"
TTNewsColors.newsCardViewBGColor = "<HEX_COLOR_CODE>"

```
